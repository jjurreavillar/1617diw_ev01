Siguiendo las bases de la distribución marcadas en Diseño inicial.png, habrá que tener en cuenta especialmente la distribución de los colores.
Se intentará jugar con los colores complementarios a fin de obtener un diseño agradable a la vista.

Es imprescindible cargar las hojas de estilo cuanto antes en el tag head.
Si fuera necesario utilizar código JavaSscript, será preferible situarlo justo antes de cerrar el tag body.

La página principal, index.html, estará dedicada al currículum en sí, mientras que f1.html servirá para hospedar el formulario.

El menú deberá de indicar con algún tipo de señal visual (como un botón hundido o en relieve) la página actual que está visitando el usuario.
